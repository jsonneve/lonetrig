#!/usr/bin/env python3

"""
Tools for manipulating text,
both strings and text in files.
"""
import os
import tempfile
import io



def make_nice_line(qtys, sep='\t', just=10):
    """
    Return a nice printable string so that
    the quantities are in table form.
    """
    return ''.join([str(qty).ljust(just) + sep for qty in qtys])

def make_line(qtys, sep='\t'):
    """
    Return a line with the quantities
    separated by sep.
    """
    length_sep = len(sep)
    return ''.join([str(qty) + sep for qty in qtys])[:-length_sep]

def write_and_replace(template, destination, replacements, order=None):
    r"""(str, str, dict) -> str
    Read out template and save to destination with
    the keys of replacements replaced by the corresponding
    values.
    >>> template = 'testing_temp.dat'
    >>> destination = 'testing_dest.dat'
    >>> fopen = open(template, 'w')
    >>> written = fopen.write('In this file\nI will replace\nsome words')
    >>> fopen.close()
    >>> replacements = {'replace': 'swap', 'words': 'letters', 'file': 'text'}
    >>> write_and_replace(template, destination, replacements)
    'testing_dest.dat'
    >>> fopen = open(destination, 'r')
    >>> fopen.read()
    'In this text\nI will swap\nsome letters'
    """
    # Check if the template given is already opened. If so, read directly.
    if isinstance(template, io.IOBase):
        source = template
    else:
        # If not, open the template for reading:
        source = open(template, 'r')
    dest = open(destination, 'w')
    line = source.readline()
    # In case replacements should happen in certain order:
    if order == None:
        order = replacements
    while line != '':
        for key in order:
            line = line.replace(key, str(replacements[key]))
        dest.write(line)
        line = source.readline()
    source.close()
    dest.close()
    return destination

def replace_in_file(filename, replacements, order=None):
    """
    Given a file, replace each key in the dictionary
    replacements with its value.
    """
    # Create a temporary file and write contents of original file to there:
    tmpfile, tmpname = tempfile.mkstemp()
    
    #tmp = os.tmpfile()
    with open(filename, 'r') as f:
        source_text = f.read()
    with open(tmpname, 'w') as t:
        t.write(source_text)
    #tmpfile.write(source_text)
    # Read temporary file 
    write_and_replace(tmpname, filename, replacements, order=order)
    return filename






