#!/usr/bin/env python
# coding: utf-8

# In[65]:


import h5py
from glob import glob
import sys, scipy
import matplotlib as mpl
from scipy.stats import chi2
import time
import math
import matplotlib.pyplot as plt
import numpy as np
import random
from sklearn.utils import shuffle
#import gpustat
#gpustat.print_gpustat()
import os
os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE" # Some versions of HDF5 require this
os.environ['CUDA_VISIBLE_DEVICES']='2' # This is to choose which GPU to use

import sys
import tensorflow as tf

import keras
from keras import backend as K
#sys.setrecursionlimit(1500)

import h5py
import numpy as np

from torch.utils.data import Dataset, DataLoader # The model is in keras, but I use pytorch for data generator


# In[2]:


INPUT_LENGTH = 10 # number of particles to consider in 1 event
BATCH_SIZE = 50

# this is the location on the caltech machine. Change it according to your case
# base_dir = '/bigdata/shared/TOPCLASS2018/BSMAnomaly_IsoLep_lt_45_pt_gt_23_NEW/SMmix_qcd_tt_w_z/' 
base_dir = 'testfiles'

# I made a copy of data to lxplus: /eos/project/d/dshep/BSM_Detection/THONG_TMP/SMmix_qcd_tt_w_z
# The particles in the event are currently sorted by pT


# In[3]:


class SimpleEventSequence(Dataset):
    def __init__(self, data_x, data_y):
        self.len = data_x.shape[0]
        self.data_x = torch.from_numpy(data_x).float()
        self.data_y = torch.from_numpy(data_y)
        
    def __len__(self):
        return self.len

    def __getitem__(self, idx):
        return (self.data_x[idx], self.data_y[idx])

class EventSequence(Dataset):
    def check_data(self, file_names):
        num_data = 0
        thresholds = [0]
        for in_file_name in file_names:
            h5_file = h5py.File( in_file_name, 'r' )
            X = h5_file[self.feature_name]
            if hasattr(X, 'keys'):
                num_data += len(X[X.keys()[0]])
                thresholds.append(num_data)
            else:
                num_data += len(X)
                thresholds.append(num_data)
            h5_file.close()
        return (num_data, thresholds)

    def __init__(self, dir_name, feature_name = 'Particles', label_name = 'Labels', sequence_length=50, verbose=False):
        self.feature_name = feature_name
        self.label_name = label_name
        self.file_names = glob(dir_name+'/*.h5')
        self.num_data, self.thresholds = self.check_data(self.file_names)
        self.sequence_length = sequence_length
        self.file_index = 0
        self.h5_file = h5py.File(self.file_names[self.file_index],'r')
        self.get_data()
        self.verbose=verbose
        
    def get_data(self):
        self.X = np.array(self.h5_file.get(self.feature_name))[:,:self.sequence_length,4:7] # 4:7 is positions of pT, eta, phi
        self.Y = np.array(self.h5_file.get(self.label_name))
#         self.X[:,0] = self.X[:,0]/15.

    def is_numpy_array(self, data):
        return isinstance(data, np.ndarray)

    def get_num_samples(self, data):
        """Input: dataset consisting of a numpy array or list of numpy arrays.
            Output: number of samples in the dataset"""
        if self.is_numpy_array(data):
            return len(data)
        else:
            return len(data[0])

    def get_index(self, idx):
        """Translate the global index (idx) into local indexes,
        including file index and event index of that file"""
        file_index = next(i for i,v in enumerate(self.thresholds) if v > idx)
        file_index -= 1
        event_index = idx - self.thresholds[file_index]
        return file_index, event_index

    def get_thresholds(self):
        return self.thresholds

    def __len__(self):
        return self.num_data

    def __getitem__(self, idx):
        file_index, event_index = self.get_index(idx)
        
        if file_index != self.file_index:
            self.h5_file.close()
            self.file_index = file_index
            if self.verbose: 
                print("Opening new file {}".format(self.file_names[self.file_index]))
            self.h5_file = h5py.File(self.file_names[self.file_index],'r')
            self.get_data()
            self.X, self.Y = shuffle(self.X, self.Y)
        #return [self.X[event_index], np.argmax(self.Y[event_index])]
        return (self.X[event_index], self.X[event_index]) 


# In[4]:


from keras.models import Model
from keras.layers import Input, GRU, LSTM, Dense, Reshape, Lambda, Concatenate
from keras.callbacks import EarlyStopping, ReduceLROnPlateau, TerminateOnNaN

InputLayer = Input(shape=(INPUT_LENGTH,3))
enc = LSTM(30, activation='relu', recurrent_activation='hard_sigmoid')(InputLayer)
enc_output = Dense(3, activation='relu')(enc)

dec = Dense(30, activation='relu')(enc_output)
dec = Dense(INPUT_LENGTH, activation='relu')(dec)
dec = Reshape((INPUT_LENGTH,1))(dec)
dec_output_pt = LSTM(1, return_sequences=True)(dec)
dec_output_eta = LSTM(1, return_sequences=True)(dec)
dec_output_phi = LSTM(1, return_sequences=True)(dec)

dec_output = Concatenate(axis=2)([dec_output_pt, dec_output_eta, dec_output_phi])

####
model = Model(inputs=InputLayer, outputs=dec_output)
model.compile(optimizer='adam', loss='mse')
model.summary()


# In[5]:



train_loader = DataLoader(EventSequence(dir_name=base_dir+'/train/',
                                    feature_name ='Particles', label_name = 'Labels', sequence_length=INPUT_LENGTH, verbose=False), 
                                    batch_size = BATCH_SIZE, shuffle=False,num_workers=3)
    
val_loader = DataLoader(EventSequence(dir_name=base_dir+'/val/',
                        feature_name ='Particles',label_name = 'Labels', sequence_length=INPUT_LENGTH, verbose=False), 
                        batch_size = BATCH_SIZE, shuffle=False,num_workers=3)

# Turn this data loader into a generator
def cycle(iterable):
    while True:
        for x in iterable:
            yield x
            
train_iter = iter(cycle(train_loader))
val_iter = iter(cycle(val_loader))

history = model.fit_generator(train_iter,
        steps_per_epoch=len(train_loader),
        epochs=10,
        validation_data=val_iter,
        validation_steps=len(val_loader),
        #callbacks=[ReduceLROnPlateau(monitor='val_loss', factor=0.1, patience=2, verbose=1),
        #TerminateOnNaN()]
                             )


# In[6]:


seven = h5py.File('testfiles/train/007.h5')


# In[7]:


seven.keys()


# In[8]:


seven['Labels']


# In[9]:


seven['HLF']


# In[10]:


seven['Particles']


# In[53]:





# In[11]:


particles = seven['Particles']


# In[12]:


particles[0]


# In[13]:


particles.shape


# In[14]:


input_particles = particles[:,:10,4:7]


# In[32]:


output_particles = model.predict(particles[:,:10,4:7])


# In[33]:


output_particles.shape


# In[34]:


print(output_particles[1])
print(input_particles[1])


# In[79]:


plt.figure()
maxpt_out = [max([part[0] for part in event]) for event in output_particles]
maxpt_in = [max([part[0] for part in event]) for event in input_particles]

#plt.hist([ptout * max(maxpt_in) for ptout in maxpt_out], alpha=0.5, bins=20)
plt.hist(maxpt_in, alpha=0.5, bins=100)
#plt.hist(maxpt_out)


# In[72]:


mpl.rcParams.keys() 


# In[81]:


mpl.rc('lines', linewidth=3)
mpl.rc('xtick', labelsize=20)
mpl.rc('ytick', labelsize=20)
mpl.rc('legend', fontsize=18)
mpl.rc('figure', titlesize=20)
mpl.rc('axes', labelsize=20)
mpl.rc('axes', titlesize=20)


plt.figure()
plt.title("Loss history")
plt.plot(history.history['loss'], label='loss')
plt.plot(history.history['val_loss'], label='val_loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend()
plt.savefig('hello.png', bbox_inches='tight')


#help(dec_output)
#dec_output.get_shape()
#dec_output[2].get_shape()
#dec_output[2][0].get_shape()
#dec_output[2][0][0].shape
#output_graph = dec_output.graph
#help(dec_output_pt)
#dec_output_pt.consumers()
#
#
#dec_output_pt.Print()
#model.predict(x_train)





