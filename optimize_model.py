#!/usr/bin/env python3
# cc0 copyleft -- public domain

# coding: utf-8



import h5py
from glob import glob
import sys, scipy
import matplotlib as mpl
from scipy.stats import chi2
import time
import math
import matplotlib.pyplot as plt
import numpy as np
import random
from sklearn.utils import shuffle
import subprocess
import text_manipulation as tm
import shutil as sh



plotdir = "/afs/desy.de/user/s/sonnevej/www/pruning/"
index_template = "index_template_pruning.html"

#import gpustat
#gpustat.print_gpustat()
import os
os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE" # Some versions of HDF5 require this
os.environ['CUDA_VISIBLE_DEVICES']='2' # This is to choose which GPU to use

import sys
import tensorflow as tf

import keras
from keras import backend as K
#sys.setrecursionlimit(1500)
from keras import regularizers

import h5py
import numpy as np

from torch.utils.data import Dataset, DataLoader # The model is in keras, but I use pytorch for data generator

print(keras.__version__)


# In[10]:


INPUT_LENGTH = 10 # number of particles to consider in 1 event
BATCH_SIZE = 500


#mounted_dir = subprocess.check_output(['./mount_dirs.sh'])
#print(mounted_dir)
# this is the location on the caltech machine. Change it according to your case
#base_dir = '/bigdata/shared/TOPCLASS2018/BSMAnomaly_IsoLep_lt_45_pt_gt_23_NEW/SMmix_qcd_tt_w_z/'
#bsm_dir = '/bigdata/shared/TOPCLASS2018/BSMAnomaly_IsoLep_lt_45_pt_gt_23_NEW/sorted_Pt_Ato4l_Htn_Htt_LQ_100k'
# with GFAL FS mount /eos/project/d/dshep/BSM_Detection/THONG_TMP under
# $MOUNTDIR
#mountdir = os.environ.get('MOUNTDIR')
# Standard model data:
# /eos/project/d/dshep/BSM_Detection/THONG_TMP/SMmix_qcd_tt_w_z
#sm_dir = os.path.join(mountdir, 'SMmix_qcd_tt_w_z')
#bsm_dir = os.path.join(mountdir, 'sorted_Pt_Ato4l_Htn_Htt_LQ')
#base_dir = sm_dir
# /eos/project/d/dshep/BSM_Detection/THONG_TMP/sorted_Pt_Ato4l_Htn_Htt_LQ
#base_dir = 'testfiles'
#bsm_dir = 'testfiles/bsm'
base_dir = 'files/sm'
bsm_dir = 'files/bsm'
# I made a copy of data to lxplus: /eos/project/d/dshep/BSM_Detection/THONG_TMP/SMmix_qcd_tt_w_z
# The particles in the event are currently sorted by pT


# In[3]:


class SimpleEventSequence(Dataset):
    def __init__(self, data_x, data_y):
        self.len = data_x.shape[0]
        self.data_x = torch.from_numpy(data_x).float()
        self.data_y = torch.from_numpy(data_y)

    def __len__(self):
        return self.len

    def __getitem__(self, idx):
        return (self.data_x[idx], self.data_y[idx])

class EventSequence(Dataset):
    def check_data(self, file_names):
        num_data = 0
        thresholds = [0]
        for in_file_name in file_names:
            print("opening " + in_file_name)
            h5_file = h5py.File( in_file_name, 'r' )
            X = h5_file[self.feature_name]
            if hasattr(X, 'keys'):
                num_data += len(X[X.keys()[0]])
                thresholds.append(num_data)
            else:
                num_data += len(X)
                thresholds.append(num_data)
            h5_file.close()
        return (num_data, thresholds)

    def __init__(self, dir_name, feature_name = 'Particles', label_name = 'Labels', sequence_length=50, verbose=False):
        self.feature_name = feature_name
        self.label_name = label_name
        self.file_names = glob(dir_name+'/*.h5')
        self.num_data, self.thresholds = self.check_data(self.file_names)
        self.sequence_length = sequence_length
        self.file_index = 0
        self.h5_file = h5py.File(self.file_names[self.file_index],'r')
        self.get_data()
        self.verbose=verbose

    def get_data(self):
        self.X = np.array(self.h5_file.get(self.feature_name))[:,:self.sequence_length,4:7] # 4:7 is positions of pT, eta, phi
        self.Y = np.array(self.h5_file.get(self.label_name))
#        self.X[:,0] = self.X[:,0]/15.

    def is_numpy_array(self, data):
        return isinstance(data, np.ndarray)

    def get_num_samples(self, data):
        """Input: dataset consisting of a numpy array or list of numpy arrays.
            Output: number of samples in the dataset"""
        if self.is_numpy_array(data):
            return len(data)
        else:
            return len(data[0])

    def get_index(self, idx):
        """Translate the global index (idx) into local indexes,
        including file index and event index of that file"""
        file_index = next(i for i,v in enumerate(self.thresholds) if v > idx)
        file_index -= 1
        event_index = idx - self.thresholds[file_index]
        return file_index, event_index

    def get_thresholds(self):
        return self.thresholds

    def __len__(self):
        return self.num_data

    def __getitem__(self, idx):
        file_index, event_index = self.get_index(idx)

        if file_index != self.file_index:
            self.h5_file.close()
            self.file_index = file_index
            if self.verbose:
                print("Opening new file {}".format(self.file_names[self.file_index]))
            self.h5_file = h5py.File(self.file_names[self.file_index],'r')
            self.get_data()
            self.X, self.Y = shuffle(self.X, self.Y)
        #return [self.X[event_index], np.argmax(self.Y[event_index])]
        X_ = self.X[event_index]
        # Scale down pT
        X = X_
        X[:,0] = X_[:,0]/10.
        Y_ = self.Y[event_index]
        if not np.any(X): # empty array, go to the next event
            print("Empty event. Skipping.")
            return self.__getitem__(idx+1)
        return (X, X)


class EventLabelSequence(EventSequence):

    def __getitem__(self, idx):
        file_index, event_index = self.get_index(idx)

        if file_index != self.file_index:
            self.h5_file.close()
            self.file_index = file_index
            if self.verbose:
                print("Opening new file {}".format(self.file_names[self.file_index]))
            self.h5_file = h5py.File(self.file_names[self.file_index],'r')
            self.get_data()
            self.X, self.Y = shuffle(self.X, self.Y)
        #return [self.X[event_index], np.argmax(self.Y[event_index])]
        X_ = self.X[event_index]
        # Scale down pT
        X = X_
        X[:,0] = X_[:,0]/10.
        Y_ = self.Y[event_index]
        if not np.any(X): # empty array
            print("Empty event. Skipping.")
            return self.__getitem__(idx+1)
        return (X, Y_)



# In[6]:


from keras.models import Model
from keras.layers import Input, GRU, LSTM, Dense, Reshape, Lambda, Concatenate, Flatten
from keras.callbacks import EarlyStopping, ReduceLROnPlateau, TerminateOnNaN

InputLayer = Input(shape=(INPUT_LENGTH,3))


if __name__ == "__main__":
    for nnodes in [500]:#, 400, 300, 200, 100]: #[500, 400, 300, 200, 100]:

        #kernel_regularizer = regularizers.l2(kernel_regularizer),
        #nnodes = 500
        n_layers_encode = 0
        kernel_regularizer = 0.01
        first_encode_nnodes = ''
        first_encode_nnodes = 200
        enc = Dense(first_encode_nnodes,
                activation='relu', kernel_regularizer=regularizers.l2(kernel_regularizer))(InputLayer)
        n_layers_encode += 1
        enc_output = Flatten()(enc) # 3d to 2d
        second_encode_nnodes = ''
        second_encode_nnodes = 400
        enc_output = Dense(second_encode_nnodes, activation='relu',
                kernel_regularizer=regularizers.l2(kernel_regularizer))(enc_output)
        n_layers_encode += 1
        third_encode_nnodes = 1000
        enc_output = Dense(third_encode_nnodes,
                activation='relu', kernel_regularizer=regularizers.l2(kernel_regularizer))(enc_output)
        n_layers_encode += 1

        n_layers_decode = 0
        first_decode_nnodes = ''
        first_decode_nnodes = 200
        dec = Dense(first_decode_nnodes, activation='relu',
                kernel_regularizer=regularizers.l2(kernel_regularizer))(enc_output)
        n_layers_decode += 1
        dec = Dense(INPUT_LENGTH, activation='relu',
                kernel_regularizer=regularizers.l2(kernel_regularizer))(dec)
        n_layers_decode += 1
        dec = Reshape((INPUT_LENGTH,1))(dec)
        second_decode_nnodes =  ''
        # second_decode_nnodes = 200
        # dec = Dense(second_decode_nnodes, activation='relu')(dec)
        # n_layers_decode += 1
        third_decode_nnodes = 200
        dec = Dense(third_decode_nnodes, activation='relu',
                kernel_regularizer=regularizers.l2(kernel_regularizer))(dec)
        n_layers_decode += 1


        dec_output_pt = Dense(1)(dec)
        dec_output_eta = Dense(1)(dec)
        dec_output_phi = Dense(1)(dec)

        dec_output = Concatenate(axis=2)([dec_output_pt, dec_output_eta, dec_output_phi])

        ####

        def custom_loss(y_truth, y_pred):

            return K.mean(K.mean(K.mean(K.abs(y_pred-y_truth), axis=-1), axis=-1), axis=-1)


        model = Model(inputs=InputLayer, outputs=dec_output)
        model.compile(optimizer='adam', loss=custom_loss)
        print(model.summary())




        train_loader = DataLoader(EventSequence(dir_name=base_dir+'/train/',
                                            feature_name ='Particles', label_name = 'Labels', sequence_length=INPUT_LENGTH, verbose=False),
                                            batch_size = BATCH_SIZE, shuffle=False,num_workers=3)

        val_loader = DataLoader(EventSequence(dir_name=base_dir+'/val/',
                                feature_name ='Particles',label_name = 'Labels', sequence_length=INPUT_LENGTH, verbose=False),
                                batch_size = BATCH_SIZE, shuffle=False,num_workers=3)

        # Turn this data loader into a generator
        def cycle(iterable):
            while True:
                for x in iterable:
                    yield [x[0].numpy(), x[1].numpy()]

        train_iter = iter(cycle(train_loader))
        val_iter = iter(cycle(val_loader))

        n_epochs = 1
        n_epochs = 200
        if baseline_test:
            history = None
        else:
            history = model.fit_generator(train_iter,
                    steps_per_epoch=len(train_loader),
                    #epochs=200,
                    epochs=n_epochs,
                    validation_data=val_iter,
                    validation_steps=len(val_loader),
                    callbacks=[
                        #ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=3, verbose=1),
                        TerminateOnNaN(),
                        EarlyStopping(patience=5,verbose=1)
                              ])


        newBATCH_SIZE = BATCH_SIZE * 10

        sm_val_loader = DataLoader(EventLabelSequence(dir_name=base_dir+'/val/',
                                feature_name ='Particles',label_name = 'Labels', sequence_length=INPUT_LENGTH, verbose=False),
                                batch_size = newBATCH_SIZE, shuffle=False,num_workers=3)

        def eval_loss(true, pred):
            return np.array(np.sum(np.sum(np.absolute(pred - true), axis=-1),axis=-1))

        for batch_idx, data in enumerate(sm_val_loader):
            x = data[0].numpy()
            output_sm = model.predict_on_batch(x)
            _loss_sm = eval_loss(x, output_sm)

            if batch_idx == 0:
                loss_sm = _loss_sm
                out_sm = output_sm
                in_sm = x
            else:
                loss_sm = np.concatenate((loss_sm,_loss_sm))
                out_sm = np.concatenate((out_sm,output_sm))
                in_sm = np.concatenate((in_sm,x))

        print(loss_sm.shape)

        # set plot params
        mpl.rc('lines', linewidth=3)
        mpl.rc('xtick', labelsize=20)
        mpl.rc('ytick', labelsize=20)
        mpl.rc('legend', fontsize=18)
        mpl.rc('figure', titlesize=20)
        mpl.rc('axes', labelsize=20)
        mpl.rc('axes', titlesize=20)


        # In[9]:
        if 'test' in base_dir:
            datatype = 'testdata'
        else:
            datatype = 'all_data_all_bsm'
        plot_name =  datatype + '_' + str(n_layers_decode ) + "_decodelayers_"
        plot_name +="_kernel_regularizerl2_eq_" + str(kernel_regularizer) + "_"
        plot_name += str(n_layers_encode) + "_encodelayers_"
        plot_name += str(first_encode_nnodes) + "_" + str(second_encode_nnodes)
        plot_name += "_" + str(third_encode_nnodes) +  "_enc_output_nnodes_"
        plot_name += str(first_decode_nnodes) + "_" + str(second_decode_nnodes)
        plot_name += str(third_decode_nnodes) + "_decode_nnodes_"
        if baseline_test:
            plot_name += 'baselinetest_'
            n_actual_epochs = 0
        else:
            n_actual_epochs = len(history.history['loss'])
        plot_name += str(n_actual_epochs) + "_epochs_"
        plot_path = os.path.join(plotdir, plot_name)
        figwidth = 200
        indextext = """
        <div class='pic'>
        <h3><a href='weights_normed.png'>weights normalized to maximum weight</a></h3><a href='weights_normed.png'><img
        src='weights_normed.png' style='border: none; width: figurewidthpx; '></a></div><div
        class='pic'><p>
        <h3><a href='weights.png'>Weights</a></h3><a href='weights.png'><img
        src='weights.png' style='border: none; width: figurewidthpx; '></a></div><div
        class='pic'><p>
        <h3><a href='roccurve.png'>ROC curve</a></h3><a href='roccurve.png'><img
        src='roccurve.png' style='border: none; width: figurewidthpx; '></a></div><div
        class='pic'><p>
        <h3><a href='bsmvssm.png'>reconstruction loss</a></h3><a
        href='bsmvssm.png'><img src='bsmvssm.png' style='border: none; width: figurewidthpx;
        '></a></div><div class='pic'><p>
        <h3><a href='smin_smout_pt.png'>pT</a></h3><a href='smin_smout_pt.png'><img
        src='smin_smout_pt.png' style='border: none; width: figurewidthpx; '></a></div><div
        class='pic'><p>
        <h3><a href='smin_smout_phi.png'>phi</a></h3><a href='smin_smout_phi.png'><img
        src='smin_smout_phi.png' style='border: none; width: figurewidthpx; '></a></div><div
        class='pic'><p>
        <h3><a href='smin_smout_eta.png'>eta</a></h3><a href='smin_smout_eta.png'><img
        src='smin_smout_eta.png' style='border: none; width: figurewidthpx; '></a></div><div
        class='pic'><p>
        </div>
        <h5><a href='fullh5model.txt'>model</a></h3>
        <h5><a href='modelsummary.txt'>summary</a><p><br><br>
        """
        indextext += "<!-- insert your figure above -->"
        indextext = indextext.replace("figurewidth", str(figwidth))
        replacetext = "<!-- insert your figure above -->"


        plotadd = "fullh5model"
        plotdest = plot_path + plotadd + '.h5'
        model.save(plotdest)

        plt.figure(figsize=(8,15))
        layernumber = 0
        for layer in model.layers:
            plt.hist(layer.get_weights(), label="layer " +
                    str(layernumber), alpha = 0.5, lw=3)
            layernumber +=1
        plt.ylabel("# entries")
        plt.xlabel("weight")
        plt.xscale('log')
        plt.legend(loc='best')
        plotadd = "weights"
        plotdest = plot_path + plotadd + '.png'
        plt.savefig(plotdest, bbox_inches='tight')
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        plt.close()


        plt.figure(figsize=(8,15))
        layernumber = 0
        for layer in model.layers:
            layweights = layer.get_weights()
            max_weight = max(layweights)
            plt.hist([layweight/max_weight for layweight in layweights], label="layer " +
                    str(layernumber), alpha = 0.5, lw=3)
            layernumber +=1
        plt.ylabel("# entries")
        plt.xlabel("weight/maxweight")
        plt.xscale('log')
        plt.legend(loc='best')
        plotadd = "weights_normed"
        plotdest = plot_path + plotadd + '.png'
        plt.savefig(plotdest, bbox_inches='tight')
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        plt.close()

        plt.hist(in_sm[:,:,0].flatten(), bins=50, range=(0,10), label='SM In')
        plt.hist(out_sm[:,:,0].flatten(),histtype='step',bins=50, range=(0,10), label='SM Out')
        #plt.hist(in_sm[:,:,0].flatten(), bins=50, range=(0,100), label='SM In')
        plt.legend(loc='best')
        plt.xlabel('PT')
        plotadd = "smin_smout_pt"
        plotdest = plot_path + plotadd + '.png'
        plt.savefig(plotdest, bbox_inches='tight')
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        #plt.show()

        plt.hist(in_sm[:,:,1].flatten(), bins=50, range=(-5,5), label='SM In')
        plt.hist(out_sm[:,:,1].flatten(),histtype='step',bins=50,range=(-5,5),  label='SM Out')
        #plt.hist(in_sm[:,:,1].flatten(), bins=50, range=(0,100), label='SM In')
        plt.legend(loc='best')
        plt.xlabel('Eta')
        plotadd = 'smin_smout_eta'
        plotdest = plot_path + plotadd + '.png'
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        plt.savefig(plotdest, bbox_inches='tight')

        #plt.show()

        plt.hist(in_sm[:,:,2].flatten(), bins=50, range=(-3.2, 3.2),  label='SM In')
        plt.hist(out_sm[:,:,2].flatten(),histtype='step',bins=50,range=(-3.2, 3.2), label='SM Out')
        plt.legend(loc='best')
        plt.xlabel('Phi')
        #plt.show()
        plotadd = 'smin_smout_phi'
        plotdest = plot_path + plotadd + '.png'
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        plt.savefig(plotdest, bbox_inches='tight')


        # In[14]:


        bsm_loader = DataLoader(EventLabelSequence(dir_name=bsm_dir+'/train/',
                                feature_name ='Particles',label_name = 'Labels', sequence_length=INPUT_LENGTH, verbose=False),
                                batch_size = newBATCH_SIZE, shuffle=False,num_workers=3)


        for batch_idx, data in enumerate(bsm_loader):
            x = data[0].numpy()
            y = data[1].numpy()
            output_bsm = model.predict_on_batch(x)
            _loss_bsm = eval_loss(x, output_bsm)
            if batch_idx == 0:
                loss_bsm = _loss_bsm
                out_bsm = output_bsm
                in_bsm = x
                label_bsm = y
            else:
                loss_bsm = np.concatenate((loss_bsm,_loss_bsm))
                out_bsm = np.concatenate((out_bsm,output_bsm))
                in_bsm = np.concatenate((in_bsm,x))
                label_bsm = np.concatenate((label_bsm,y))
        print(loss_bsm.shape)


        # In[16]:


        label_bsm_index = np.argmax(label_bsm, axis=1)
        print(label_bsm_index.shape)
        # In[25]:


        BSM = ['Ato4l','ChHToTauNu','HToTauTau','LQ']
        plt.figure(figsize=(10,6))
        plt.hist(loss_sm,bins=100,label='SM', normed=True)
        for i,name in enumerate(BSM):
            plt.hist(loss_bsm[label_bsm_index==i],bins=100,label=name, histtype='step', normed=True)
        plt.legend(loc='best',fontsize=16)
        plt.xlabel('Reconstruction Loss', fontsize=16)
        plt.yscale('log')
        #plt.,fontsize=16)
        plotadd = 'bsmvssm'
        plotdest = plot_path + plotadd + '.png'
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        plt.savefig(plotdest, bbox_inches='tight')


        # In[21]:


        p_SM = np.logspace(base=10, start=-5, stop=0, num=100)
        p_SM[-1] = 0.999

        f_ROC, ax_arr_ROC = plt.subplots(1,1, figsize=(10,10))
        ax_arr_ROC.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')

        ax_arr_ROC.set_xlim([1e-4, 1.0])
        ax_arr_ROC.set_ylim([1e-4, 1.05])
        ax_arr_ROC.set_xlabel('SM efficiency')
        ax_arr_ROC.ticklabel_format()
        ax_arr_ROC.set_ylabel('BSM efficiency')
        ax_arr_ROC.set_yscale('log')
        ax_arr_ROC.set_xscale('log')
        ax_arr_ROC.grid()

        # First sort the SM distribution
        sm_sort = np.argsort(loss_sm)
        sm_re_loss = loss_sm[sm_sort]

        # Find out the sm threshold values corresponding to each percentile in p_SM
        frac = np.cumsum(np.ones_like(sm_re_loss)/len(sm_re_loss)) # an array of fraction (1/n, 2/n, etc.)
        indices_of_thresholds = np.argmax(frac > np.atleast_2d(1-p_SM).T, axis=1) # which element closest to each value in p_SM
        sm_thresholds = sm_re_loss[indices_of_thresholds] # what are the corresponding threshold values

        for i,name in enumerate(BSM):
            bsm = loss_bsm[label_bsm_index==i]
            # For each of the threshold above, compute the fraction of BSM events that would be selected at that SM threshold
            bsm_frac = np.float64(np.sum(bsm > np.atleast_2d(sm_thresholds).T, axis=1, dtype=np.float128)/len(bsm))
            # Voila
            ax_arr_ROC.plot(p_SM,bsm_frac,label=name)

        ax_arr_ROC.legend(loc="lower right", fontsize=16)
        plotadd = 'roccurve'
        plotdest = plot_path + plotadd + '.png'
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        plt.savefig(plotdest, bbox_inches='tight')



        # In[ ]:


        index_file = "index_pruning.html"

        plotadd = 'modelsummary'
        modelsummary_dest = plot_path + plotadd + '.txt'
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        #indextext += "<br>"
        with open(modelsummary_dest, 'w') as summaryfile:
            model.summary(print_fn=lambda x: summaryfile.write(x + '\n'))
        tm.write_and_replace(index_template, index_file, {replacetext: indextext})
        sh.copyfile(index_file, index_template)
        sh.copyfile(index_file, os.path.join(plotdir, "index.html"))

        #unmounted_dir = subprocess.check_output(['./unmount_dirs.sh'])
        #print(unmounted_dir)
