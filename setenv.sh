#!/usr/bin/env zsh
source /etc/profile.d/modules.sh
module load anaconda/3
source activate Tensorflow-GPU
#cd l1vae
#python SimpleSeq2Seq.py


#gfalFS_umount ${MOUNTDIR}
#root://eoscms.cern.ch:1094/eos/project//d/dshep/BSM_Detection/THONG_TMP/SMmix_qcd_tt_w_z
# gfalFS -s  ${MOUNTDIR} root://eosatlas.cern.ch:1094/eos/atlas/user/?/YOURUSERNAME

# the remote directory should be available now locally
# ls ${MOUNTDIR}
# → a  b  c  d  e  f  g  h  i  j  k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z
#
# # unmount the path at the end
# # else the job cleanup might cause data losses or worse!
#
# gfalFS_umount ${MOUNTDIR}

# sonnevej@naf-cms-gpu01 2019-05-20 16:20:24
# /afs/desy.de/user/s/sonnevej/l1vae % conda info -e
# conda environments:
#
#                          /nfs/dust/cms/user/sonnevej/anaconda/anaconda2/envs/tensorflow
# base                  *  /opt/anaconda3/5.2
# Python2                  /opt/anaconda3/5.2/envs/Python2
# Tensorflow-GPU           /opt/anaconda3/5.2/envs/Tensorflow-GPU
# pytorch                  /opt/anaconda3/5.2/envs/pytorch
# 
# sonnevej@naf-cms-gpu01 2019-05-20 16:20:26
# /afs/desy.de/user/s/sonnevej/l1vae % source activate Tensorflow-GPU
