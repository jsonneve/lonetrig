#!/usr/bin/env python
# cc0 copyleft -- public domain
# coding: utf-8


import h5py
from glob import glob
import sys, scipy
import matplotlib as mpl
from scipy.stats import chi2
import time
import math
import matplotlib.pyplot as plt
import numpy as np
import random
from sklearn.utils import shuffle
import subprocess
import text_manipulation as tm
import shutil as sh
import datetime

from sklearn.utils import shuffle

plotdir = "/afs/desy.de/user/s/sonnevej/www/binary/"
index_template = "index_template.html"
index_file = "index_binary.html"


import gpustat
print("GPU stats:")
gpustat.print_gpustat()
print("")
import os
os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE" # Some versions of HDF5 require this
os.environ['CUDA_VISIBLE_DEVICES']='4' # This is to choose which GPU to use

import sys
import tensorflow as tf

import keras
from keras import backend as K
#sys.setrecursionlimit(1500)
from keras import regularizers

import h5py
import numpy as np

import torch
from torch.utils.data import Dataset, DataLoader # The model is in keras, but I use pytorch for data generator

print("Keras version: " + str(keras.__version__))

# old
#INPUT_LENGTH = 10 # number of particles to consider in 1 event
#BATCH_SIZE = 10

# new
INPUT_LENGTH = 20 # number of particles to consider in 1 event
INPUT_FEATURE = 4 # number of features. use pt, eta, phi, and pid
BATCH_SIZE = 30
PT_SCALE = 10
MAX_EPOCH = 200
# Maurizio's Delphes data
# this is the location on the caltech machine. I copied the file to
# /eos/project/d/dshep/Thong_tmp/ on lxplus
#base_dir = '/bigdata/shared/L1AnomalyDetection/qcd_lepFilter_13TeV/' 
#base_file = '/bigdata/shared/L1AnomalyDetection/qcd_all.npy'
#bsm_dir = '/bigdata/shared/L1AnomalyDetection/Ato4l_lepFilter_13TeV/'
#bsm_file = '/bigdata/shared/L1AnomalyDetection/Ato4l.npy'
#model_file = 'DelphesModel.torch'

# particle is ordered: MET + 10 e + 10 mu + 20 jet
# We will take 4 ele + 4 muon + 12 jets = 20 objects in total

PARTICLE_TO_USE = np.asarray([False]*41)
PARTICLE_TO_USE[1:5] = True # 4 ele
PARTICLE_TO_USE[11:15] = True # 4 muon
PARTICLE_TO_USE[21:33] = True # 12 jets
print("Number of particles to use: ", sum(PARTICLE_TO_USE))
FLAT_FEATURES = INPUT_LENGTH*INPUT_FEATURE


# this is the location on the caltech machine. Change it according to your case
#base_dir = '/bigdata/shared/TOPCLASS2018/BSMAnomaly_IsoLep_lt_45_pt_gt_23_NEW/SMmix_qcd_tt_w_z/'
#bsm_dir = '/bigdata/shared/TOPCLASS2018/BSMAnomaly_IsoLep_lt_45_pt_gt_23_NEW/sorted_Pt_Ato4l_Htn_Htt_LQ_100k'
#base_dir = 'files/sm'
#bsm_dir = 'files/bsm'
#base_dir = 'testfiles'
#bsm_dir = 'testfiles/bsm'
base_dir = '/nfs/dust/cms/user/sonnevej/l1vae/files/sm'
bsm_dir = '/nfs/dust/cms/user/sonnevej/l1vae/files/bsm'
base_file = '/nfs/dust/cms/user/sonnevej/lonetrig/files/qcd_all.npy'
bsm_file = '/nfs/dust/cms/user/sonnevej/lonetrig/files/Ato4l.npy'
#base_dir = '/nfs/dust/cms/user/sonnevej/lonetrig/'
#base_dir = '/nfs/dust/cms/user/sonnevej/l1vae/testfiles/'
#bsm_dir = '/nfs/dust/cms/user/sonnevej/l1vae/testfiles/bsm/'
# I made a copy of data to lxplus: /eos/project/d/dshep/BSM_Detection/THONG_TMP/SMmix_qcd_tt_w_z
# The particles in the event are currently sorted by pT
testdata = False #True
baseline_test = False
testdata = True
baseline_test = True
ntestevents = int(1e7) #1000000
no_binary = True


class SimpleEventSequence(Dataset):
    def __init__(self, data_x):
        self.len = data_x.shape[0]
        self.data_x = torch.from_numpy(data_x).float()
        #self.data_y = torch.from_numpy(data_y)
    def __len__(self):
        return self.len

    def __getitem__(self, idx):
        # (20,4) to (80)
        x = self.data_x[idx].contiguous().view(FLAT_FEATURES)
        return x, x
        # return self.data_x[idx].contiguous().view(FLAT_FEATURES) # pytorch
# Old data loader
#class SimpleEventSequence(Dataset):
#    def __init__(self, data_x, data_y):
#        self.len = data_x.shape[0]
#        self.data_x = torch.from_numpy(data_x).float()
#        self.data_y = torch.from_numpy(data_y)
#
#    def __len__(self):
#        return self.len
#
#    def __getitem__(self, idx):
#        return (self.data_x[idx], self.data_y[idx])

class EventSequence(Dataset):
    def check_data(self, file_names):
        num_data = 0
        thresholds = [0]
        for in_file_name in file_names:
            h5_file = h5py.File( in_file_name, 'r' )
            X = h5_file[self.feature_name]
            if hasattr(X, 'keys'):
                num_data += len(X[X.keys()[0]])
                thresholds.append(num_data)
            else:
                num_data += len(X)
                thresholds.append(num_data)
            h5_file.close()
        return (num_data, thresholds)

    def __init__(self, dir_name, feature_name = 'Particles', label_name = 'Labels', sequence_length=50, verbose=False):
        self.feature_name = feature_name
        self.label_name = label_name
        self.file_names = glob(dir_name+'/*.h5')
        self.num_data, self.thresholds = self.check_data(self.file_names)
        self.sequence_length = sequence_length
        self.file_index = 0
        self.h5_file = h5py.File(self.file_names[self.file_index],'r')
        self.get_data()
        self.verbose=verbose

    def get_data(self):
        self.X = np.array(self.h5_file.get(self.feature_name))[:,:self.sequence_length,4:7] # 4:7 is positions of pT, eta, phi
        self.Y = np.array(self.h5_file.get(self.label_name))
#        self.X[:,0] = self.X[:,0]/15.

    def is_numpy_array(self, data):
        return isinstance(data, np.ndarray)

    def get_num_samples(self, data):
        """Input: dataset consisting of a numpy array or list of numpy arrays.
            Output: number of samples in the dataset"""
        if self.is_numpy_array(data):
            return len(data)
        else:
            return len(data[0])

    def get_index(self, idx):
        """Translate the global index (idx) into local indexes,
        including file index and event index of that file"""
        file_index = next(i for i,v in enumerate(self.thresholds) if v > idx)
        file_index -= 1
        event_index = idx - self.thresholds[file_index]
        return file_index, event_index

    def get_thresholds(self):
        return self.thresholds

    def __len__(self):
        return self.num_data

    def __getitem__(self, idx):
        file_index, event_index = self.get_index(idx)

        if file_index != self.file_index:
            self.h5_file.close()
            self.file_index = file_index
            if self.verbose:
                print("Opening new file {}".format(self.file_names[self.file_index]))
            self.h5_file = h5py.File(self.file_names[self.file_index],'r')
            self.get_data()
            self.X, self.Y = shuffle(self.X, self.Y)
        #return [self.X[event_index], np.argmax(self.Y[event_index])]
        X_ = self.X[event_index]
        # Scale down pT
        X = X_
        X[:,0] = X_[:,0]/10.
        X = X.flatten()
        Y_ = self.Y[event_index]
        if not np.any(X): # empty array, go to the next event
            print("Empty event. Skipping.")
            return self.__getitem__(idx+1)
        return (X, X)


class EventLabelSequence(EventSequence):

    def __getitem__(self, idx):
        file_index, event_index = self.get_index(idx)

        if file_index != self.file_index:
            self.h5_file.close()
            self.file_index = file_index
            if self.verbose:
                print("Opening new file {}".format(self.file_names[self.file_index]))
            self.h5_file = h5py.File(self.file_names[self.file_index],'r')
            self.get_data()
            self.X, self.Y = shuffle(self.X, self.Y)
        #return [self.X[event_index], np.argmax(self.Y[event_index])]
        X_ = self.X[event_index]
        # Scale down pT
        X = X_
        X[:,0] = X_[:,0]/10.
        X = X.flatten()
        Y_ = self.Y[event_index]
        if not np.any(X): # empty array
            print("Empty event. Skipping.")
            return self.__getitem__(idx+1)
        return (X, Y_)





from keras.models import Model, Sequential
from keras.layers import Input, GRU, LSTM, Dense, Reshape, Lambda, Concatenate, Flatten
from keras.layers import Input, BatchNormalization, Activation
from keras.callbacks import EarlyStopping, ReduceLROnPlateau, TerminateOnNaN
from quantized_layers import BinaryDense, TernaryDense, QuantizedDense
from quantized_ops import binary_tanh


if __name__ == "__main__":
    if True:

        # Load data new format
        all_qcd = np.load(base_file).astype(np.float32)[:,PARTICLE_TO_USE,:]
        size_qcd = len(all_qcd)
        qcd_train = all_qcd[:int(size_qcd/2)]
        qcd_val = all_qcd[int(size_qcd/2):]
        print('Shape of QCD input: ' + str(all_qcd.shape))

        x_mean = np.mean(qcd_train.reshape((-1,4)), axis=0)
        x_std = np.std(qcd_train.reshape((-1,4)), axis=0)
        print('Mean of QCD input: ' + str(x_mean))
        print('Standard dev. of QCD input: ' + str(x_std))

        if testdata:
            qcd_train = qcd_train[:ntestevents]
            qcd_val = qcd_val[:ntestevents]
        qcd_train_norm = qcd_train
        qcd_val_norm = qcd_val

        # Normalize pt, eta, phi
        for i in range(3): 
            qcd_train_norm[:,:,i] = (qcd_train[:,:,i] - x_mean[i])/(x_std[i])
            qcd_val_norm[:,:,i] = (qcd_val[:,:,i] - x_mean[i])/(x_std[i])
        train_loader = DataLoader(SimpleEventSequence(qcd_train_norm),
                batch_size = BATCH_SIZE, shuffle=False, num_workers=3)

        val_loader = DataLoader(SimpleEventSequence(qcd_val_norm), 
                batch_size = BATCH_SIZE*10, shuffle=False, num_workers=3)




        #train_loader = DataLoader(EventSequence(dir_name=base_dir+'/train/',
        #                                    feature_name ='Particles', label_name = 'Labels', sequence_length=INPUT_LENGTH, verbose=False),
        #                                    batch_size = BATCH_SIZE, shuffle=False,num_workers=3)

        #val_loader = DataLoader(EventSequence(dir_name=base_dir+'/val/',
        #                        feature_name ='Particles',label_name = 'Labels', sequence_length=INPUT_LENGTH, verbose=False),
        #                        batch_size = BATCH_SIZE, shuffle=False,num_workers=3)

        # Turn this data loader into a generator
        def cycle(iterable):
            while True:
                for x in iterable:
                    yield [x[0].numpy(), x[1].numpy()]

        train_iter = iter(cycle(train_loader))

        print("Shape of QCD input after train data loader: " + str(
            train_iter.__next__()))
        val_iter = iter(cycle(val_loader))

        #
        # binary model
        # model.add(BatchNormalization(epsilon=1e-6, momentum=0.9, name='bn0', input_shape=(16,)))
        #
        # model.add(BinaryDense(64, H=1, use_bias=False, name='fc1'))
        # model.add(BatchNormalization(epsilon=1e-6, momentum=0.9, name='bn1'))
        # model.add(Activation('relu', name='act{}'.format(1)))
        #
        # model.add(BinaryDense(32, H=1, use_bias=False, name='fc2'))
        # model.add(BatchNormalization(epsilon=1e-6, momentum=0.9, name='bn2'))
        # model.add(Activation('relu', name='act{}'.format(2)))
        #
        # model.add(BinaryDense(32, H=1, use_bias=False, name='fc3'))
        # model.add(BatchNormalization(epsilon=1e-6, momentum=0.9, name='bn3'))
        # model.add(Activation('relu', name='act{}'.format(3)))
        #
        # model.add(BinaryDense(nclasses, H=1, use_bias=False, name='output'))
        # model.add(BatchNormalization(epsilon=1e-6, momentum=0.9, name='bn'))
        #InputLayer = Input(shape=(INPUT_LENGTH*3,))
        # model.Input(shape=(INPUT_LENGTH*3,))


        if no_binary:
            ##################
            ## thong's 1680 ##
            ##################
            INPUT_LENGTH=10
            InputLayer = Input(shape=(INPUT_LENGTH*3,))
            enc = Dense(INPUT_LENGTH*2, activation='relu')(InputLayer)
            enc = Dense(INPUT_LENGTH, activation='relu')(enc)
            dec = Dense(INPUT_LENGTH*2, activation='relu')(enc)
            dec = Dense(INPUT_LENGTH*3, activation=None)(dec)
            model = Model(inputs=InputLayer, outputs=dec)
        else: 
            ##################
            ## binary dense ##
            ##################
            model = Sequential()
            model.add(BatchNormalization(epsilon=1e-6, momentum=0.9, name='bn0',
                input_shape=(FLAT_FEATURES,)))
                #input_shape=(None, FLAT_FEATURES*3/4.,)))
            model.add(BinaryDense(int(INPUT_LENGTH/2.), H=1, use_bias=False, name='fc1'))
            model.add(BatchNormalization(epsilon=1e-6, momentum=0.9, name='bn1'))
            model.add(Activation('relu', name='act{}'.format(1)))
            model.add(BinaryDense(int(INPUT_LENGTH/4.), H=1, use_bias=False,
                name='fc2'))
            model.add(BatchNormalization(epsilon=1e-6, momentum=0.9, name='bn2'))
            model.add(Activation('relu', name='act{}'.format(2)))
            model.add(BinaryDense(int(INPUT_LENGTH/4.), H=1, use_bias=False, name='fc3'))
            model.add(BatchNormalization(epsilon=1e-6, momentum=0.9, name='bn3'))
            model.add(Activation('relu', name='act{}'.format(3)))
            model.add(BinaryDense(FLAT_FEATURES, H=1, use_bias=False, name='fc4'))
            model.add(BatchNormalization(epsilon=1e-6, momentum=0.9, name='bn4'))
            model.add(Activation(None, name='act{}'.format(4)))

        def custom_loss(y_truth, y_pred):

            return K.mean(K.mean(K.mean(K.abs(y_pred-y_truth), axis=-1), axis=-1), axis=-1)



        #model = Model(inputs=InputLayer, outputs=dec)
        model.compile(optimizer='adam', loss='mae')
        model.summary()

        #n_epochs = 1
        n_epochs = 200
        if baseline_test:
            history = None
        else:
            history = model.fit_generator(train_iter,
                steps_per_epoch=len(train_loader),
                epochs=n_epochs,
                validation_data=val_iter,
                validation_steps=len(val_loader),
                callbacks=[
                    ReduceLROnPlateau(monitor='val_loss', factor=0.2, patience=3, verbose=1),
                    TerminateOnNaN(),
                    EarlyStopping(patience=7,verbose=1)
                          ])



        newBATCH_SIZE = BATCH_SIZE * 10
        # Old data format:
        # sm_val_loader = DataLoader(EventLabelSequence(dir_name=base_dir+'/val/',
        #                         feature_name ='Particles',label_name = 'Labels', sequence_length=INPUT_LENGTH, verbose=False),
        #                         batch_size = newBATCH_SIZE, shuffle=False,num_workers=3)
        # new data format:
        sm_val_loader = DataLoader(SimpleEventSequence(qcd_val_norm), 
                batch_size = BATCH_SIZE*10, shuffle=False, num_workers=3)


        def eval_loss(true, pred):
            return np.array(np.sum(np.absolute(pred - true),axis=-1))

        for batch_idx, data in enumerate(sm_val_loader):
            x = data[0].numpy()
            output_sm = model.predict_on_batch(x)
            _loss_sm = eval_loss(x, output_sm)

            if batch_idx == 0:
                loss_sm = _loss_sm
                out_sm = output_sm
                in_sm = x
            else:
                loss_sm = np.concatenate((loss_sm,_loss_sm))
                out_sm = np.concatenate((out_sm,output_sm))
                in_sm = np.concatenate((in_sm,x))

        print("Shape of standard model loss: " + str(loss_sm.shape))
        print("In sm shape, example: " + str(in_sm) + ' shape ' +
                str(in_sm.shape))
        print("out sm shape, example: " + str(out_sm) + ' shape ' +
                str(out_sm.shape))


        #font = {'family' : 'serif',
        #        'weight' : 'normal',
        #        'size'   : 25,
        #         }

        #matplotlib.rc('font', **font)
        #matplotlib.rcParams['figure.figsize'] = 12, 9
        #matplotlib.rcParams['lines.linewidth'] = 3
        #matplotlib.rcParams['xtick.labelsize'] = 16
        #matplotlib.rcParams['ytick.labelsize'] = 16
        # set plot params
        mpl.rc('lines', linewidth=3)
        mpl.rc('xtick', labelsize=20)
        mpl.rc('ytick', labelsize=20)
        mpl.rc('legend', fontsize=18)
        mpl.rc('figure', titlesize=20)
        mpl.rc('axes', labelsize=20)
        mpl.rc('axes', titlesize=20)




        # In[8]:

        # Load new data format:
        bsm = np.load(bsm_file).astype(np.float32)[:,PARTICLE_TO_USE,:]
        size_bsm = len(bsm)
        print("BSM shape: " + str(bsm.shape))

        x_mean = np.mean(bsm.reshape((-1,4)), axis=0)
        x_std = np.std(bsm.reshape((-1,4)), axis=0)
        print('Mean of BSM input: ' + str(x_mean))
        print('Standard dev. of BSM input: ' + str(x_std))

        if testdata:
            bsm = bsm[:ntestevents]
        bsm_norm = bsm

        # Normalize pt, eta, phi
        for i in range(3): 
            bsm_norm[:,:,i] = (bsm[:,:,i] - x_mean[i])/(x_std[i])
        bsm_loader = DataLoader(SimpleEventSequence(bsm_norm), 
                batch_size = BATCH_SIZE*10, shuffle=False, num_workers=3)

        # Old data format:
        # bsm_loader = DataLoader(EventLabelSequence(dir_name=bsm_dir+'/train/',
        #                         feature_name ='Particles',label_name = 'Labels', sequence_length=INPUT_LENGTH, verbose=False),
        #                         batch_size = newBATCH_SIZE, shuffle=False,num_workers=3)


        for batch_idx, data in enumerate(bsm_loader):
            x = data[0].numpy()
            y = data[1].numpy()
            output_bsm = model.predict_on_batch(x)
            _loss_bsm = eval_loss(x, output_bsm)

            if batch_idx == 0:
                loss_bsm = _loss_bsm
                out_bsm = output_bsm
                in_bsm = x
                label_bsm = y
            else:
                loss_bsm = np.concatenate((loss_bsm,_loss_bsm))
                out_bsm = np.concatenate((out_bsm,output_bsm))
                in_bsm = np.concatenate((in_bsm,x))
                label_bsm = np.concatenate((label_bsm,y))

        print("Shape of BSM loss: " + str(loss_bsm.shape))



        label_bsm_index = np.argmax(label_bsm, axis=1)
        print("Shape of BSM label index: " + str(label_bsm_index.shape))

        if 'test' in base_dir or testdata:
            datatype = 'testdata'
            datatype += '_on_' + str(ntestevents) + "_events"
        else:
            datatype = 'new_data'
        amount_params = model.count_params()
        #plot_name =  datatype + '_' + str(n_layers_decode ) + "_decodelayers_"
        #plot_name +="_kernel_regularizerl2_eq_" + str(kernel_regularizer) + "_"
        #plot_name += str(n_layers_encode) + "_encodelayers_"
        #plot_name += str(first_encode_nnodes) + "_" + str(second_encode_nnodes)
        #plot_name += "_" + str(third_encode_nnodes) +  "_enc_output_nnodes_"
        #plot_name += str(first_decode_nnodes) + "_" + str(second_decode_nnodes)
        #plot_name += str(third_decode_nnodes) + "_decode_nnodes_"
        #plot_name = datatype + '_minimal_model_thong_'
        plot_name = datetime.date.today().isoformat() + '_' 
        plot_name += str(datetime.datetime.today().hour) + 'h_'
        if no_binary:
            plot_name += datatype + '_orig_model_'
        else:
            plot_name += datatype + '_binary_dense_model_'
        if baseline_test:
            plot_name += 'baselinetest_'
            n_actual_epochs = 0
        else:
            n_actual_epochs = len(history.history['loss'])
        plot_name += str(amount_params) + '_params_'
        plot_name += str(n_actual_epochs) + "_epochs_"
        plot_path = os.path.join(plotdir, plot_name)
        figwidth = 400
        indextext = """
        <div class='pic'><p>
        <h3><a href='roccurve.png'>ROC curve</a>; <a href='modelh5.h5'>h5
        model</a>, <a href='modeljson.json'>json model</a>, <a
        href='modelsummary.txt'>summary</a></h3><a href='roccurve.png'><img
        src='roccurve.png' style='border: none; width: figurewidthpx; '></a></div><div
        class='pic'><p>
        <h3><a href='recloss.png'>reconstruction loss</a>; <a href='modelh5.h5'>h5
        model</a>, <a href='modeljson.json'>json model</a>, <a
        href='modelsummary.txt'>summary</a></h3><a
        href='recloss.png'><img src='recloss.png' style='border: none; width: figurewidthpx;
        '></a></div>
        <div class='pic'><p>
        <h3><a href='pt.png'>pT</a>; <a href='modelh5.h5'>h5
        model</a>, <a href='modeljson.json'>json model</a>, <a
        href='modelsummary.txt'>summary</a></h3><a href='pt.png'><img
        src='pt.png' style='border: none; width: figurewidthpx; '></a></div>
        <h3><a href='eta.png'>Eta</a>; <a href='modelh5.h5'>h5
        model</a>, <a href='modeljson.json'>json model</a>, <a
        href='modelsummary.txt'>summary</a></h3><a href='eta.png'><img
        src='eta.png' style='border: none; width: figurewidthpx; '></a></div>
        <h3><a href='phi.png'>Phi</a>; <a href='modelh5.h5'>h5
        model</a>, <a href='modeljson.json'>json model</a>, <a
        href='modelsummary.txt'>summary</a></h3><a href='phi.png'><img
        src='phi.png' style='border: none; width: figurewidthpx; '></a></div>
        <h3><a href='pid.png'>PID</a>; <a href='modelh5.h5'>h5
        model</a>, <a href='modeljson.json'>json model</a>, <a
        href='modelsummary.txt'>summary</a></h3><a href='pid.png'><img
        src='pid.png' style='border: none; width: figurewidthpx; '></a></div>
        <h3><a href='bsmtransversemom.png'>BSM pT</a>; <a href='modelh5.h5'>h5
        model</a>, <a href='modeljson.json'>json model</a>, <a
        href='modelsummary.txt'>summary</a></h3><a href='bsmtransversemom.png'><img
        src='bsmtransversemom.png' style='border: none; width: figurewidthpx; '></a></div>
        <p><br><br>
        """
        # <h3><a href='modelh5.h5'>h5 model</a></h3>
        # <json><a href='modeljson.json'>json model</a></h3>
        # <h3><a href='modelsummary.txt'>summary</h3></a><p><br><br>
        indextext += "<!-- insert your figure above -->"
        indextext = indextext.replace("figurewidth", str(figwidth))
        replacetext = "<!-- insert your figure above -->"



        plotadd = 'pt'
        plt.figure(figsize=(10,6))
        plotdest = plot_path + plotadd + '.png'
        #plt.hist([in_sm[i][0] for i in range(len(in_sm))], bins=50, range=(0,10), label='SM In')
        #plt.hist([out_sm[i][0] for i in range(len(out_sm))],histtype='step',bins=50, range=(0,10), label='SM Out')
        plt.hist(in_sm[:,:,0].flatten(), bins=50, range=(0,10), label='SM In')
        plt.hist(out_sm[:,:,0].flatten(),histtype='step',bins=50, range=(0,10), label='SM Out')
        #plt.hist(in_sm[:,:,0].flatten(), bins=50, range=(0,100), label='SM In')
        plt.legend(loc='best')
        plt.xlabel('$p_{T}$')
        plt.savefig(plotdest, bbox_inches='tight')
        indextext = indextext.replace(plotadd, plot_name + plotadd)

        # plt.show()

        plotadd = 'eta'
        plt.figure(figsize=(10,6))
        plotdest = plot_path + plotadd + '.png'
        #plt.hist([in_sm[i][1] for i in range(len(in_sm))], bins=50, range=(0,10), label='SM In')
        #plt.hist([out_sm[i][1] for i in range(len(out_sm))],histtype='step',bins=50, range=(0,10), label='SM Out')
        plt.hist(in_sm[:,:,1].flatten(), bins=50, range=(0,10), label='SM In')
        plt.hist(out_sm[:,:,1].flatten(),histtype='step',bins=50, range=(0,10), label='SM Out')
        # plt.hist(in_sm[:,:,0].flatten(), bins=50, range=(0,100), label='SM In')
        plt.legend(loc='best')
        plt.xlabel('$\eta$')
        plt.savefig(plotdest, bbox_inches='tight')
        indextext = indextext.replace(plotadd, plot_name + plotadd)



        plotadd = 'phi'
        plt.figure(figsize=(10,6))
        plotdest = plot_path + plotadd + '.png'
        #plt.hist([in_sm[i][2] for i in range(len(in_sm))], bins=50, range=(0,10), label='SM In')
        #plt.hist([out_sm[i][2] for i in range(len(out_sm))],histtype='step',bins=50, range=(0,10), label='SM Out')
        plt.hist(in_sm[:,:,2].flatten(), bins=50, range=(0,10), label='SM In')
        plt.hist(out_sm[:,:,2].flatten(),histtype='step',bins=50, range=(0,10), label='SM Out')
        #plt.hist(in_sm[:,:,0].flatten(), bins=50, range=(0,100), label='SM In')
        plt.legend(loc='best')
        plt.xlabel('$\phi$')
        plt.savefig(plotdest, bbox_inches='tight')
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        # plt.hist(in_sm[:,:,1].flatten(), bins=50, range=(-5,5), label='SM In')
        # plt.hist(out_sm[:,:,1].flatten(),histtype='step',bins=50,range=(-5,5),  label='SM Out')
        # #plt.hist(in_sm[:,:,1].flatten(), bins=50, range=(0,100), label='SM In')
        # plt.legend(loc='best')
        # plt.xlabel('Eta')
        # plt.show()

        plotadd = 'pid'
        plt.figure(figsize=(10,6))
        plotdest = plot_path + plotadd + '.png'
        #plt.hist([in_sm[i][3] for i in range(len(in_sm))], bins=50, range=(0,10), label='SM In')
        #plt.hist([out_sm[i][3] for i in range(len(out_sm))],histtype='step',bins=50, range=(0,10), label='SM Out')
        plt.hist(in_sm[:,:,3].flatten(), bins=50, range=(0,10), label='SM In')
        plt.hist(out_sm[:,:,3].flatten(),histtype='step',bins=50, range=(0,10), label='SM Out')
        #plt.hist(in_sm[:,:,0].flatten(), bins=50, range=(0,100), label='SM In')
        plt.legend(loc='best')
        plt.xlabel('particle ID')
        plt.savefig(plotdest, bbox_inches='tight')
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        # plt.hist(in_sm[:,:,2].flatten(), bins=50, range=(-3.2, 3.2),  label='SM In')
        # plt.hist(out_sm[:,:,2].flatten(),histtype='step',bins=50,range=(-3.2, 3.2), label='SM Out')
        # plt.legend(loc='best')
        # plt.xlabel('Phi')
        # plt.show()




        plotadd = 'modelsummary'
        plotdest = plot_path + plotadd + '.txt'
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        with open(plotdest, 'w') as summaryfile:
            model.summary(print_fn=lambda x: summaryfile.write(x + '\n'))


        plotadd = 'recloss'
        plotdest = plot_path + plotadd + '.png'
        indextext = indextext.replace(plotadd, plot_name + plotadd)

        # old
        # BSM = ['Ato4l','ChHToTauNu','HToTauTau','LQ']
        # new
        BSM = ['Ato4l']
        plt.figure(figsize=(10,6))
        plt.hist(loss_sm,bins=100,label='SM', normed=True)
        for i,name in enumerate(BSM):
            plt.hist(loss_bsm[label_bsm_index==i],bins=100,label=name, histtype='step', normed=True)
        plt.legend(loc='best')#,fontsize=16)
        plt.xlabel('Reconstruction Loss')#, fontsize=16)
        plt.yscale('log')#,fontsize=16)
        plt.savefig(plotdest, bbox_inches='tight')

        plotadd = 'bsmtransversemom'
        plotdest = plot_path + plotadd + '.png'
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        BSM = ['Ato4l']
        plt.figure(figsize=(10,6))
        plt.hist(in_bsm[:,:,0].flatten(), bins=50, range=(0,10), label='BSM in')
        plt.hist(out_bsm[:,:,0].flatten(),histtype='step',bins=50, range=(0,10),
                label='BSM out')
        plt.hist(loss_sm,bins=100,label='SM', normed=True)
        for i,name in enumerate(BSM):
            plt.hist(loss_bsm[label_bsm_index==i],bins=100,label=name, histtype='step', normed=True)
        plt.legend(loc='best')#,fontsize=16)
        plt.xlabel('$p_{T}$')
        plt.savefig(plotdest, bbox_inches='tight')



        p_SM = np.logspace(base=10, start=-5, stop=0, num=100)
        p_SM[-1] = 0.999

        f_ROC, ax_arr_ROC = plt.subplots(1,1, figsize=(10,10))
        ax_arr_ROC.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')

        ax_arr_ROC.set_xlim([1e-8, 1.0])
        ax_arr_ROC.set_ylim([1e-8, 1.05])
        ax_arr_ROC.set_xlabel('SM efficiency')#, fontdict={'size': 16})
        ax_arr_ROC.ticklabel_format()
        ax_arr_ROC.set_ylabel('BSM efficiency')#, fontdict={'size': 16})
        ax_arr_ROC.set_yscale('log')
        ax_arr_ROC.set_xscale('log')
        ax_arr_ROC.grid()

        # First sort the SM distribution
        sm_sort = np.argsort(loss_sm)
        sm_re_loss = loss_sm[sm_sort]

        # Find out the sm threshold values corresponding to each percentile in p_SM
        frac = np.cumsum(np.ones_like(sm_re_loss)/len(sm_re_loss)) # an array of fraction (1/n, 2/n, etc.)
        indices_of_thresholds = np.argmax(frac > np.atleast_2d(1-p_SM).T, axis=1) # which element closest to each value in p_SM
        sm_thresholds = sm_re_loss[indices_of_thresholds] # what are the corresponding threshold values

        for i,name in enumerate(BSM):
            bsm = loss_bsm[label_bsm_index==i]
            # For each of the threshold above, compute the fraction of BSM events that would be selected at that SM threshold
            bsm_frac = np.float64(np.sum(bsm > np.atleast_2d(sm_thresholds).T, axis=1, dtype=np.float128)/len(bsm))
            # Voila
            ax_arr_ROC.plot(p_SM,bsm_frac,label=name)

        ax_arr_ROC.legend(loc="lower right")#, fontsize=16)


        plotadd = 'roccurve'
        plotdest = plot_path + plotadd + '.png'
        indextext = indextext.replace(plotadd, plot_name + plotadd)

        plt.savefig(plotdest, bbox_inches='tight')

        # Save model
        plotadd = 'modeljson'
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        plotdest = plot_path + plotadd + '.json'
        import json
        model_json = model.to_json()
        with open(plotdest,'w') as out:
            json.dump(model_json, out)
        plotadd = 'modelh5'
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        plotdest = plot_path + plotadd + '.h5'
        model.save_weights(plotdest)


        tm.write_and_replace(index_template, index_file, {replacetext: indextext})
        sh.copyfile(index_file, index_template)
        sh.copyfile(index_file, os.path.join(plotdir, "index.html"))
