#!/usr/bin/env python3
# cc0 copyleft -- public domain

# coding: utf-8



import h5py
from glob import glob
import sys, scipy
import matplotlib as mpl
from scipy.stats import chi2
import time
import math
import matplotlib.pyplot as plt
import numpy as np
import random
from sklearn.utils import shuffle
import subprocess
import text_manipulation as tm
import shutil as sh



plotdir = "/afs/desy.de/user/s/sonnevej/www/pruning/"
index_template = "index_template_pruning.html"

#import gpustat
#gpustat.print_gpustat()
import os
os.environ["HDF5_USE_FILE_LOCKING"] = "FALSE" # Some versions of HDF5 require this
os.environ['CUDA_VISIBLE_DEVICES']='2' # This is to choose which GPU to use

import sys
import tensorflow as tf

import keras
from keras import backend as K
#sys.setrecursionlimit(1500)
from keras import regularizers

import h5py
import numpy as np

from torch.utils.data import Dataset, DataLoader # The model is in keras, but I use pytorch for data generator

print(keras.__version__)


# In[10]:


INPUT_LENGTH = 10 # number of particles to consider in 1 event
BATCH_SIZE = 500


#mounted_dir = subprocess.check_output(['./mount_dirs.sh'])
#print(mounted_dir)
# this is the location on the caltech machine. Change it according to your case
#base_dir = '/bigdata/shared/TOPCLASS2018/BSMAnomaly_IsoLep_lt_45_pt_gt_23_NEW/SMmix_qcd_tt_w_z/'
#bsm_dir = '/bigdata/shared/TOPCLASS2018/BSMAnomaly_IsoLep_lt_45_pt_gt_23_NEW/sorted_Pt_Ato4l_Htn_Htt_LQ_100k'
# with GFAL FS mount /eos/project/d/dshep/BSM_Detection/THONG_TMP under
# $MOUNTDIR
#mountdir = os.environ.get('MOUNTDIR')
# Standard model data:
# /eos/project/d/dshep/BSM_Detection/THONG_TMP/SMmix_qcd_tt_w_z
#sm_dir = os.path.join(mountdir, 'SMmix_qcd_tt_w_z')
#bsm_dir = os.path.join(mountdir, 'sorted_Pt_Ato4l_Htn_Htt_LQ')
#base_dir = sm_dir
# /eos/project/d/dshep/BSM_Detection/THONG_TMP/sorted_Pt_Ato4l_Htn_Htt_LQ
#base_dir = 'testfiles'
#bsm_dir = 'testfiles/bsm'
base_dir = 'files/sm'
bsm_dir = 'files/bsm'
# I made a copy of data to lxplus: /eos/project/d/dshep/BSM_Detection/THONG_TMP/SMmix_qcd_tt_w_z
# The particles in the event are currently sorted by pT


if __name__ == "__main__":

    # Use the startup Python helper:
    # command line argument parsing

    argv = sys.argv
    progName = os.path.basename(argv.pop(0))
    import argparse

    parser = argparse.ArgumentParser(prog=progName, description="A Simple DAQ using the pxarCore API.")
    parser.add_argument('--h5file', '-h', metavar="H5FILE", help="The model stored in an h5 file.")
    #parser.add_argument('--outfile', '-o', metavar="OUTFILE", help="The hdf5 file to output all the data to.")
    #parser.add_argument('--verbosity', '-v', metavar="LEVEL", default="INFO", help="The output verbosity set in the pxarCore API.")
    args = parser.parse_args(argv)
    modelname = args.h5file



        plt.figure(figsize=(8,15))
        layernumber = 0
        for layer in model.layers:
            plt.hist(layer.get_weights(), label="layer " +
                    str(layernumber), alpha = 0.5, lw=3)
            layernumber +=1
        plt.ylabel("# entries")
        plt.xlabel("weight")
        plt.xscale('log')
        plt.legend(loc='best')
        plotadd = "weights"
        plotdest = plot_path + plotadd + '.png'
        plt.savefig(plotdest, bbox_inches='tight')
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        plt.close()


        plt.figure(figsize=(8,15))
        layernumber = 0
        for layer in model.layers:
            layweights = layer.get_weights()
            max_weight = max(layweights)
            plt.hist([layweight/max_weight for layweight in layweights], label="layer " +
                    str(layernumber), alpha = 0.5, lw=3)
            layernumber +=1
        plt.ylabel("# entries")
        plt.xlabel("weight/maxweight")
        plt.xscale('log')
        plt.legend(loc='best')
        plotadd = "weights_normed"
        plotdest = plot_path + plotadd + '.png'
        plt.savefig(plotdest, bbox_inches='tight')
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        plt.close()

        plt.hist(in_sm[:,:,0].flatten(), bins=50, range=(0,10), label='SM In')
        plt.hist(out_sm[:,:,0].flatten(),histtype='step',bins=50, range=(0,10), label='SM Out')
        #plt.hist(in_sm[:,:,0].flatten(), bins=50, range=(0,100), label='SM In')
        plt.legend(loc='best')
        plt.xlabel('PT')
        plotadd = "smin_smout_pt"
        plotdest = plot_path + plotadd + '.png'
        plt.savefig(plotdest, bbox_inches='tight')
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        #plt.show()

        plt.hist(in_sm[:,:,1].flatten(), bins=50, range=(-5,5), label='SM In')
        plt.hist(out_sm[:,:,1].flatten(),histtype='step',bins=50,range=(-5,5),  label='SM Out')
        #plt.hist(in_sm[:,:,1].flatten(), bins=50, range=(0,100), label='SM In')
        plt.legend(loc='best')
        plt.xlabel('Eta')
        plotadd = 'smin_smout_eta'
        plotdest = plot_path + plotadd + '.png'
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        plt.savefig(plotdest, bbox_inches='tight')

        #plt.show()

        plt.hist(in_sm[:,:,2].flatten(), bins=50, range=(-3.2, 3.2),  label='SM In')
        plt.hist(out_sm[:,:,2].flatten(),histtype='step',bins=50,range=(-3.2, 3.2), label='SM Out')
        plt.legend(loc='best')
        plt.xlabel('Phi')
        #plt.show()
        plotadd = 'smin_smout_phi'
        plotdest = plot_path + plotadd + '.png'
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        plt.savefig(plotdest, bbox_inches='tight')


        # In[14]:


        bsm_loader = DataLoader(EventLabelSequence(dir_name=bsm_dir+'/train/',
                                feature_name ='Particles',label_name = 'Labels', sequence_length=INPUT_LENGTH, verbose=False),
                                batch_size = newBATCH_SIZE, shuffle=False,num_workers=3)


        for batch_idx, data in enumerate(bsm_loader):
            x = data[0].numpy()
            y = data[1].numpy()
            output_bsm = model.predict_on_batch(x)
            _loss_bsm = eval_loss(x, output_bsm)
            if batch_idx == 0:
                loss_bsm = _loss_bsm
                out_bsm = output_bsm
                in_bsm = x
                label_bsm = y
            else:
                loss_bsm = np.concatenate((loss_bsm,_loss_bsm))
                out_bsm = np.concatenate((out_bsm,output_bsm))
                in_bsm = np.concatenate((in_bsm,x))
                label_bsm = np.concatenate((label_bsm,y))
        print(loss_bsm.shape)


        # In[16]:


        label_bsm_index = np.argmax(label_bsm, axis=1)
        print(label_bsm_index.shape)
        # In[25]:


        BSM = ['Ato4l','ChHToTauNu','HToTauTau','LQ']
        plt.figure(figsize=(10,6))
        plt.hist(loss_sm,bins=100,label='SM', normed=True)
        for i,name in enumerate(BSM):
            plt.hist(loss_bsm[label_bsm_index==i],bins=100,label=name, histtype='step', normed=True)
        plt.legend(loc='best',fontsize=16)
        plt.xlabel('Reconstruction Loss', fontsize=16)
        plt.yscale('log')
        #plt.,fontsize=16)
        plotadd = 'bsmvssm'
        plotdest = plot_path + plotadd + '.png'
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        plt.savefig(plotdest, bbox_inches='tight')


        # In[21]:


        p_SM = np.logspace(base=10, start=-5, stop=0, num=100)
        p_SM[-1] = 0.999

        f_ROC, ax_arr_ROC = plt.subplots(1,1, figsize=(10,10))
        ax_arr_ROC.plot([0, 1], [0, 1], color='navy', lw=2, linestyle='--')

        ax_arr_ROC.set_xlim([1e-4, 1.0])
        ax_arr_ROC.set_ylim([1e-4, 1.05])
        ax_arr_ROC.set_xlabel('SM efficiency')
        ax_arr_ROC.ticklabel_format()
        ax_arr_ROC.set_ylabel('BSM efficiency')
        ax_arr_ROC.set_yscale('log')
        ax_arr_ROC.set_xscale('log')
        ax_arr_ROC.grid()

        # First sort the SM distribution
        sm_sort = np.argsort(loss_sm)
        sm_re_loss = loss_sm[sm_sort]

        # Find out the sm threshold values corresponding to each percentile in p_SM
        frac = np.cumsum(np.ones_like(sm_re_loss)/len(sm_re_loss)) # an array of fraction (1/n, 2/n, etc.)
        indices_of_thresholds = np.argmax(frac > np.atleast_2d(1-p_SM).T, axis=1) # which element closest to each value in p_SM
        sm_thresholds = sm_re_loss[indices_of_thresholds] # what are the corresponding threshold values

        for i,name in enumerate(BSM):
            bsm = loss_bsm[label_bsm_index==i]
            # For each of the threshold above, compute the fraction of BSM events that would be selected at that SM threshold
            bsm_frac = np.float64(np.sum(bsm > np.atleast_2d(sm_thresholds).T, axis=1, dtype=np.float128)/len(bsm))
            # Voila
            ax_arr_ROC.plot(p_SM,bsm_frac,label=name)

        ax_arr_ROC.legend(loc="lower right", fontsize=16)
        plotadd = 'roccurve'
        plotdest = plot_path + plotadd + '.png'
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        plt.savefig(plotdest, bbox_inches='tight')



        # In[ ]:


        index_file = "index_pruning.html"

        plotadd = 'modelsummary'
        modelsummary_dest = plot_path + plotadd + '.txt'
        indextext = indextext.replace(plotadd, plot_name + plotadd)
        #indextext += "<br>"
        with open(modelsummary_dest, 'w') as summaryfile:
            model.summary(print_fn=lambda x: summaryfile.write(x + '\n'))
        tm.write_and_replace(index_template, index_file, {replacetext: indextext})
        sh.copyfile(index_file, index_template)
        sh.copyfile(index_file, os.path.join(plotdir, "index.html"))

        #unmounted_dir = subprocess.check_output(['./unmount_dirs.sh'])
        #print(unmounted_dir)
